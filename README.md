# Conway's Game of Life

## How to play
[Conway's Game of Life](https://brave-clarke-bdb336.netlify.com)

## Project setup
```
npm install
```
### Compiles and hot-reloads for development
```
npm run serve
```
### Compiles and minifies for production
```
npm run build
```
### Compiles and runs unit tests
```
npm run test
```
### Lints and fixes files
```
npm run lint
```
