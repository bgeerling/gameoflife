import {direction, patterns, game} from '../../src/js/game'

describe('Grid', () => {

  it('initiates grid', () => {
    const expected = [
      [0,0,0],
      [0,0,0],
      [0,0,0]
    ]

    const grid = game.init(3,3)

    expect(grid).toEqual(expected)
  })

  it('initiates oblong grid', () => {
    const expected = [
      [0,0,0,0,0,0],
      [0,0,0,0,0,0],
      [0,0,0,0,0,0]
    ]

    const grid = game.init(6,3)

    expect(grid).toEqual(expected)
  })

  it('gets grid values by index correctly', () => {
    const grid = game.init(3,3)

    expect(grid[0][0]).toEqual(0)
    expect(grid[0][1]).toEqual(0)
    expect(grid[0][2]).toEqual(0)
    expect(grid[1][0]).toEqual(0)
    expect(grid[1][1]).toEqual(0)
    expect(grid[1][2]).toEqual(0)
    expect(grid[2][0]).toEqual(0)
    expect(grid[2][1]).toEqual(0)
    expect(grid[2][2]).toEqual(0)

    expect(() => grid[3][0]).toThrowError(TypeError, "Cannot read property '0' of undefined")
    expect(grid[0][5]).toBeUndefined()
  })

  it('sets grid by index correctly', () => {
    const expected = [
      [1,0,0],
      [0,0,1],
    ]

    const grid = game.init(3,2)
    grid[0][0] = 1
    grid[1][2] = 1

    expect(grid).toEqual(expected)
  })

  it('transform grid to an array of (x,y) values correctly', () => {
    const expected = [
      [0,0,'A0'],
      [0,1,'A1'],
      [0,2,'A2'],
      [1,0,'B0'],
      [1,1,'B1'],
      [1,2,'B2'],
      [2,0,'C0'],
      [2,1,'C1'],
      [2,2,'C2'],
      [3,0,'D0'],
      [3,1,'D1'],
      [3,2,'D2'],
    ]

    const grid = [
      ['A0','A1','A2'],
      ['B0','B1','B2'],
      ['C0','C1','C2'],
      ['D0','D1','D2'],
    ]

    let rects = []

    grid.forEach((column, columnIndex) => {
      column.forEach((cell, rowIndex) => {
        rects.push([columnIndex, rowIndex, cell])
      })
    })

    expect(rects).toEqual(expected)
  })

  it('updates grid with forEach correctly', () => {
    const expected = [
      [1,1,1],
      [1,1,1],
      [1,1,1]
    ]

    const grid = game.init(3,3)

    grid.forEach((column, columnIndex) => {
      column.forEach((cell, rowIndex) => {
        if (cell === 0) {
          grid[columnIndex][rowIndex] = 1
        }
      })
    })

    expect(grid).toEqual(expected)
  })

})

describe('Direction', () => {

  let grid = []

  beforeAll(function() {
    /*
    NW N NE
    W  0 E
    SW S SE
    */
    grid = [
      ['NW','W','SW'],
      ['N',0,'S'],
      ['NE','E','SE']
    ]
  })

  afterAll(function() {
    grid = []
  })

  it('gets east neighbor correctly', () => {
    expect(
      game.neighbor(grid, 1, 1, direction.E)
    ).toEqual('E')
  })

  it('gets northeast neighbor correctly', () => {
    expect(
      game.neighbor(grid, 1, 1, direction.NE)
    ).toEqual('NE')
  })

  it('gets north neighbor correctly', () => {
    expect(
      game.neighbor(grid, 1, 1, direction.N)
    ).toEqual('N')
  })

  it('gets northwest neighbor correctly', () => {
    expect(
      game.neighbor(grid, 1, 1, direction.NW)
    ).toEqual('NW')
  })

  it('gets west neighbor correctly', () => {
    expect(
      game.neighbor(grid, 1, 1, direction.W)
    ).toEqual('W')
  })
 
  it('gets southwest neighbor correctly', () => {
    expect(
      game.neighbor(grid, 1, 1, direction.SW)
    ).toEqual('SW')
  })

  it('gets south neighbor correctly', () => {
    expect(
      game.neighbor(grid, 1, 1, direction.S)
    ).toEqual('S')
  })

  it('gets southeast neighbor correctly', () => {
    expect(
      game.neighbor(grid, 1, 1, direction.SE)
    ).toEqual('SE')
  })

})

describe('Edgecase top left of canvas', () => {
  let grid = []
  const columnIndex = 0
  const rowIndex = 0

  beforeAll(function() {
    /*
    0  E  W
    S  SE SW
    N  NE NW
    */
    grid = [
      [0,'S','N'],
      ['E','SE','NE'],
      ['W','SW','NW']
    ]

    if (grid[columnIndex][rowIndex] !== 0) {
      throw 'test setup failed'
    }
  })

  afterAll(function() {
    grid = []
  })

  it('gets edgecase east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.E)
    ).toEqual('E')
  })
  
  it('gets edgecase north east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.NE)
    ).toEqual('NE')
  })

  it('gets edgecase north neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.N)
    ).toEqual('N')
  })

  it('gets edgecase north west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.NW)
    ).toEqual('NW')
  })

  it('gets edgecase west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.W)
    ).toEqual('W')
  })

  it('gets edgecase south west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.SW)
    ).toEqual('SW')
  })

  it('gets edgecase south neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.S)
    ).toEqual('S')
  })

  it('gets edgecase south east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.SE)
    ).toEqual('SE')
  })
  
})

describe('Edgecase top right of canvas', () => {
  let grid = []
  const columnIndex = 2
  const rowIndex = 0

  beforeAll(function() {
    /*
    E  W  0
    SE SW S
    NE NW N
    */
    grid = [
      ['E','SE','NE'],
      ['W','SW','NW'],
      [0,'S','N']
    ]

    if (grid[columnIndex][rowIndex] !== 0) {
      throw 'test setup failed'
    }
  })

  afterAll(function() {
    grid = []
  })

  it('gets edgecase east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.E)
    ).toEqual('E')
  })
  
  it('gets edgecase north east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.NE)
    ).toEqual('NE')
  })

  it('gets edgecase north neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.N)
    ).toEqual('N')
  })

  it('gets edgecase north west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.NW)
    ).toEqual('NW')
  })

  it('gets edgecase west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.W)
    ).toEqual('W')
  })

  it('gets edgecase south west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.SW)
    ).toEqual('SW')
  })

  it('gets edgecase south neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.S)
    ).toEqual('S')
  })

  it('gets edgecase south east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.SE)
    ).toEqual('SE')
  })
  
})

describe('Edgecase bottom left of canvas', () => {
  let grid = []
  const columnIndex = 0
  const rowIndex = 2

  beforeAll(function() {
    /*
    S  SE SW
    N  NE NW
    0  E  W
    */
    grid = [
      ['S','N',0],
      ['SE','NE','E'],
      ['SW','NW','W']
    ]

    if (grid[columnIndex][rowIndex] !== 0) {
      throw 'test setup failed'
    }
  })

  afterAll(function() {
    grid = []
  })

  it('gets edgecase east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.E)
    ).toEqual('E')
  })
  
  it('gets edgecase north east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.NE)
    ).toEqual('NE')
  })

  it('gets edgecase north neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.N)
    ).toEqual('N')
  })

  it('gets edgecase north west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.NW)
    ).toEqual('NW')
  })

  it('gets edgecase west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.W)
    ).toEqual('W')
  })

  it('gets edgecase south west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.SW)
    ).toEqual('SW')
  })

  it('gets edgecase south neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.S)
    ).toEqual('S')
  })

  it('gets edgecase south east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.SE)
    ).toEqual('SE')
  })
  
})

describe('Edgecase bottom right of canvas', () => {
  let grid = []
  const columnIndex = 2
  const rowIndex = 2

  beforeAll(function() {
    /*
    SE SW S
    NE NW N
    E  W  0
    */
    grid = [
      ['SE','NE','E'],
      ['SW','NW','W'],
      ['S','N', 0]
    ]

    if (grid[columnIndex][rowIndex] !== 0) {
      throw 'test setup failed'
    }
  })

  afterAll(function() {
    grid = []
  })

  it('gets edgecase east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.E)
    ).toEqual('E')
  })
  
  it('gets edgecase north east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.NE)
    ).toEqual('NE')
  })

  it('gets edgecase north neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.N)
    ).toEqual('N')
  })

  it('gets edgecase north west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.NW)
    ).toEqual('NW')
  })

  it('gets edgecase west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.W)
    ).toEqual('W')
  })

  it('gets edgecase south west neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.SW)
    ).toEqual('SW')
  })

  it('gets edgecase south neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.S)
    ).toEqual('S')
  })

  it('gets edgecase south east neighbor correctly', () => {
    expect(
      game.neighbor(grid, columnIndex, rowIndex, direction.SE)
    ).toEqual('SE')
  })
  
})

describe('Neighbors', () => {

  it('sums neighbors only', () => {
    const expected = 4

    const grid = [
      [1,0,1],
      [1,1,0],
      [0,1,0]
    ]

    const rowIndex = 1
    const columnIndex = 1

    const actual = game.neighbors(grid, columnIndex, rowIndex)

    expect(actual).toEqual(expected)
  }) 

  it('sums edgecase neighbors correctly', () => {
    const expected = 8

    const grid = [
      [1,1,0],
      [1,1,1],
      [1,1,1]
    ]

    const columnIndex = 0
    const rowIndex = 2

    if (grid[columnIndex][rowIndex] !== 0) {
      throw 'test setup failed'
    }

    const actual = game.neighbors(grid, columnIndex, rowIndex)

    expect(actual).toEqual(expected)
  }) 
  
})

describe('Add Pattern', () => {

  it('adds a cell', () => {

    const grid = game.init(2,4)
    
    game.addPattern(grid, 2, 0, patterns.stilllife.block)
  })
  
  it('adds still life block to grid', () => {
    /*
    0 0 1 1
    0 0 1 1
    */
    const expected = [
      [0,0],
      [0,0],
      [1,1],
      [1,1]
    ]

    const grid = game.init(2,4)
    
    game.addPattern(grid, 2, 0, patterns.stilllife.block)

    expect(grid).toEqual(expected)
  })

})

describe('Iterate', () => {

  const ui = {
    drawcell(columnIndex, rowIndex) {
      return {columnIndex, rowIndex}
    }
  }

  const grid = [
    [0,0,0,0,0],
    [0,0,1,0,0],
    [0,0,1,0,0],
    [0,0,1,0,0],
    [0,0,0,0,0]
  ]

  it('calls callback correct number of times', () => {
    spyOn(ui, 'drawcell')

    game.iterate(grid, ui.drawcell)

    expect(ui.drawcell).toHaveBeenCalledTimes(3)
  })

  it('calls callback arguments correctly', () => {
    const grid = [
      [0,0,0,0],
      [0,1,1,0],
      [0,1,1,0],
      [0,0,0,0]
    ]
    spyOn(ui, 'drawcell')

    game.iterate(grid, ui.drawcell)

    expect(ui.drawcell).toHaveBeenCalledTimes(4)
    expect(ui.drawcell.calls.argsFor(0)).toEqual([1,1])
    expect(ui.drawcell.calls.argsFor(1)).toEqual([1,2])
    expect(ui.drawcell.calls.argsFor(2)).toEqual([2,1])
    expect(ui.drawcell.calls.argsFor(3)).toEqual([2,2])
  })

  it('iterates blinker pattern correctly', () => {
    const expectedState1 = [
      [0,0,0,0,0],
      [0,0,1,0,0],
      [0,0,1,0,0],
      [0,0,1,0,0],
      [0,0,0,0,0]
    ]

    const expectedState2 = [
      [0,0,0,0,0],
      [0,0,0,0,0],
      [0,1,1,1,0],
      [0,0,0,0,0],
      [0,0,0,0,0]
    ]

    const grid = game.init(5,5)
    game.addPattern(grid, 1, 2, patterns.oscillator.blinker)
    expect(grid).toEqual(expectedState1)

    game.iterate(grid, ui.drawcell)
    expect(grid).toEqual(expectedState2)

    game.iterate(grid, ui.drawcell)
    expect(grid).toEqual(expectedState1)
  })

})