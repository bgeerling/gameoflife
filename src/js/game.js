export const direction = {
  E:{x:1,y:0},
  NE:{x:1,y:1},
  N:{x:0,y:1},
  NW:{x:-1,y:1},
  W:{x:-1,y:0},
  SW:{x:-1,y:-1},
  S:{x:0,y:-1},
  SE:{x:1,y:-1},
}

export const patterns = {
  stilllife : {
    block: [
      {x:0,y:0},
      {x:0,y:1},
      {x:1,y:0},
      {x:1,y:1}
    ],
    beehive: [
      {x:0,y:1},
      {x:0,y:2},
      {x:1,y:0},
      {x:1,y:3},
      {x:2,y:1},
      {x:2,y:2}
    ]
  },
  oscillator: { 
    blinker: [
      {x:0,y:0},
      {x:0,y:1},
      {x:0,y:2}
    ],
    toad: [
      {x:0,y:1},
      {x:0,y:2},
      {x:0,y:3},
      {x:1,y:0},
      {x:1,y:1},
      {x:1,y:2}
    ],
    beacon: [
      {x:0,y:0},
      {x:0,y:1},
      {x:1,y:0},
      {x:1,y:1},
      {x:2,y:2},
      {x:2,y:3},
      {x:3,y:2},
      {x:3,y:3}
    ]
  },
  spaceship: {
    glider: [
      {x:0,y:1},
      {x:1,y:2},
      {x:2,y:0},
      {x:2,y:1},
      {x:2,y:2}
    ],
    lightweight: [
      {x:0,y:1},
      {x:0,y:2},
      {x:1,y:0},
      {x:1,y:1},
      {x:1,y:2},
      {x:1,y:3},
      {x:2,y:0},
      {x:2,y:1},
      {x:2,y:3},
      {x:2,y:4},
      {x:3,y:2},
      {x:3,y:3}
    ]
  }
}

export const game = {
  columnTemplate(numberOfColumns) {
    let columnTemplate = new Array()

    for(let rowIndex = 0; rowIndex < numberOfColumns; rowIndex++) {
      columnTemplate.push(0)
    }

    return columnTemplate
  },
  init(numberOfColumns, numberOfRows) {
    let grid = new Array()
    let columnTemplate = this.columnTemplate(numberOfColumns)

    for(let rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
      grid.push(columnTemplate.slice())
    }

    return grid
  },
  neighbor(grid, columnIndex, rowIndex, direction) {

    // lookup by (x,y) coordinate
    let neighborColumnIndex = columnIndex + direction.x
    let neighborRowIndex  = rowIndex - direction.y

    if (neighborRowIndex < 0) {
      neighborRowIndex = grid[0].length-1
    }

    if (neighborRowIndex > grid[0].length-1) {
      neighborRowIndex = 0
    }

    if (neighborColumnIndex < 0) {
      neighborColumnIndex = grid.length-1
    }

    if (neighborColumnIndex > grid.length-1) {
      neighborColumnIndex = 0
    }

    return grid[neighborColumnIndex][neighborRowIndex]
  },
  neighbors(grid, columnIndex, rowIndex) {

    let neighborCount = 0

    for(let element of Object.keys(direction)) {
      neighborCount += this.neighbor(grid, columnIndex, rowIndex, direction[element]) === 1
    }

    return neighborCount
    
  },
  addPattern(grid, columnIndex, rowIndex, pattern) {

    pattern.forEach(element => {
      grid[columnIndex + element.y][rowIndex + element.x] = 1
    })

  },
  iterate(grid, callback) {
    let neighbors = 0
    let updates = []

    grid.forEach((column, columnIndex) => {
      column.forEach((cell, rowIndex) => {

      neighbors = game.neighbors(grid, columnIndex, rowIndex)

      if (cell === 0 && neighbors === 3) {
        updates.push({columnIndex, rowIndex, value: 1})
        callback(columnIndex, rowIndex)
      } else if (cell === 1 && (neighbors < 2 || neighbors > 3)) {
        updates.push({columnIndex, rowIndex, value: 0})
      } else if (cell === 1) {
        callback(columnIndex, rowIndex)
      }

        neighbors = 0
      })
    })

    updates.forEach(update => {
      grid[update.columnIndex][update.rowIndex] = update.value
    })
    updates = []

  }
}